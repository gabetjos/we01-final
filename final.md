# Final WE01

Ce travail, dans sa globalité, est mis à disposition selon les termes de la [Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
Vous pouvez copier, diffuser et transformer ce contenu.

## Le Web est-il devenu trop compliqué ?

### Internet un grand fouillis ?

#### Un système multiagents en plusieurs couches

Internet est un *réseau de réseaux*. Ainsi dans sa définition même on voit qu'un nombre très important d'acteurs sont en jeu. Cela mène à un certain nombre de difficultés. Il faut que chacun de ses acteurs puisse communiquer de façon efficace et précise en évitant au maximum les erreurs et la concurrence. 
Ce sont en fait les difficultés que l'on retrouve dans l'étude générale des systèmes multiagents qui sont :
* asynchrone (des processus différents se déroyules) ;
* parallèle (en similtané) ;
* distribué (sur plusieurs machines) 

mais doivent aussi être 
* résilient ; 
* flexible (réagir vite, de prendre des initiatives, interagire).

Pour répondre à ces attentes, un modèle en plusieurs couches a été développé. Il s'agit du **modèle OSI** qui contient 7 couches, chacune agissant à un niveau différent (du matériel jusqu'à une couche applicative). Chaque couche à des protocoles qui lui sont propres afin de permettre la communication entre les différents agents. Par exemple les couche bas niveau on vu apparaitre les protocoles **TCP/IP** permettent le découpage de l'information en *paquets* et le bon acheminement de ces paquets.
Pourl'internet à plus haut niveau et notamment le web, le **protocole HTTP** permet des requêtes entre un serveur et un client envoyant des messages sous forme de numéros signalant une erreur (4xx côté serveur, 5xx côté client) ou un bon fonctionnement (2xx). 
Ce modèle en plusieurs couches rend le système fonctionnel et est quasi-nécessaire si l'on veut faire interagir des milliards d'entités, mais c'est aussi une source de complexité de fonctionnement.

#### Des acteurs économiques spécialisé

En plus de cela, de nombreux acteurs économiques spécialisés sont nécessaires au maintien de l'internet.
Il existe un aspect technique et matériel : pose de *câble sous-marin*, envoi de satellites, réparation de routeurs, qui nécessitent des entreprises ultras spécialisées.

Mais il y a aussi des aspects organisationnel et sécuritaire : attribution des noms de domaines, certificats pour le **HTTPS** (HTTP + TSL)... Par exemple les DNS sont chargés de faire le lien entre adresse d'un serveur et nom de domaine. 
Les agences de certification, elles, sont chargées de fournir des certificats de confiance assurant qu'un site web est bien celui qu'il prétend être. C'est donc une grosse responsabilité, celle de la sécurité des gens.

Enfin il y a aussi de très nombreux préctataires de services (dont les **GAFAM**) quyi proposent des services en ligne mais aussi des services plus materiels comme l'utilisation de leurs serveurs pour l'hébergement de site web ou le stockage de data.

### Un web utlra technologiques ?

#### de nombreuses technologies

Le nombre de technologies pour faire du développement web a très largement explosé depuis la création du web. Au début seul des langages de balise telle que les premières versions d'HTML existaient, mais pour rendre le web plus attractif et augmenter les possibilités, ce sont développe de nombreux langage adapté au web. 
> HTML, CSS et Javascript sont bien plus anciens

Mais ont vue le jour en plus du JavaScript de très nombreux dérivé sous forme de *bibliothèques* et d'*API* (comme l'API GoogleMaps). 

Avec une multitude langage et formalisme à savoir interprété, couplé avec le grand nombre de protocoles; l'affirmation sur le fait que 
> [la complexification du web] se traduit par des logiciels beaucoup plus complexes, donc elle réduit la concurrence, très peu d'organisation pouvant aujourd'hui développer un navigateur Web

semble justifiée.
Les pages web sont maintenant plus lourdes comme on peut allé le vérifier sur [HTTP Archives](https://httparchive.org/). 

Dans ce contexte on constate que les navigateurs se ressemblent de plus en plus
> Edge et Safari utilisent le même moteur de rendu, WebKit 

Mais comment éviter un tel constat alors que le nombre de technologies à maitriser se voit croissant ? Le cout de développement et de maintien d'un tel moteur ne peut que croitre. Et au-delà de ça, on ne peut que constater l'échec passé de la tentative de Microsoft avec Internet Explorer de faire différemment. Est-ce même souhaitable ? Des rendus différents obligent les développeurs à plus de travail, ne rendant ainsi pas le web plus facile à comprendre (plusieurs codes pour plusieurs rendus selon le navigateur). 

#### Trop dépendant de ces technologies...

Si le développement de technologies parait comme une source de bonnes évolutions, l'*obsolescence* induite est-elle source de questions ?

> C'est justement un des problèmes du Web qu'un logiciel datant de 2015 ne fonctionne plus.



 #### Le modèle économique du web

Les sites web sont de plus en plus chargés aussi, car le modèle économique sous-jacent le rend plus lourd.

> Derrière votre écran, l'affichage de la moindre page Web va déclencher d'innombrables opérations sur des machines que vous ne voyez pas

La *publicité* et le *tracking* (qui sont par ailleurs liés, car l'un permet à l'autre d'être plus efficace). Demande à des acteurs une communication en temps réel alors mêm que le client se connectant à un site peut être quasi inerte. 

> ces fonctions ne sont pas forcément dans l'intérêt de l'utilisateur

Ces technologies ne sont en fait pas utiliser dans l'intérêt de l'utilisateur, mais plus dans celui du développeur voir des publicitaires ou même des agences de publicité (et là aussi les géants du web on la part belle, comme avec Google Ads service de publicité en ligne de Google pour son site web).


### Des outils pour construire une alterntive, les chaines éditoriales

Le projet **Gemini** a pour objectif de proposer une alternative au web en ce qui concerne la recherche d'information. Son but est de proposer un système plus simple que le web.

Dans le cadre d'une diffusion simplifiée d'information, on doit s'interroger sur la façon de créer des documents (contenant de l'information).

Les chaines éditoriales sont des outils d'éditions multimédias qui proposent de se concentrer sur la sémantique ainsi que sur la structure d'un document (grâce à des descripteurs sémantiques et structurels) plutôt que sur la forme. La forme vient dans un second temps grâce à l'application d'un format canonique.
C'est le paradigm **What You See Is What You Mean** .Ce que l'on voit est ce que l'on veut dire ! La forme est directement dépendante du fond.

Ces outils prennent tout leur sens dans un contexte de simplification du web. En effet ils proposent de nombreux avantages. Notamment le **polymorphisme** permet de créer à partir d'un même contenu structuré plusieurs documents sur plusieurs supports.  La conception est modulaire, on peut dorénavant **ré-éditer** et comprendre simplement le sens d'un contenu puisque c'est le sens et la structure qui priment.
Le gain de temps est aussi important puisque la forme est applicable facilement dans un second temps. Et la séparation des métiers permet une spécialisation et un travail plus approfondi. Cette séparation des métiers et justement due à la séparation entre la forme et le fond.

Ainsi à partir d'un même travail on peut partager de multiple documents sous de multiples formats et ceux en conservant une même sémantique.


